clear all
T = 500;
n = 4;
m = 1;

noiseHorizon = 100;
previewHorizon = 20;

regWMeFix = zeros(previewHorizon,T);
regWLi = zeros(previewHorizon,T);
regAvgMeFix = zeros(previewHorizon,T);
regAvgLi = zeros(previewHorizon,T);
regContFix = zeros(previewHorizon,T);

%%
numMonte = 200;

epsilon = 100;
for numExp = 1:numMonte
    regWMeFix = zeros(previewHorizon,1);
    regWLi = zeros(previewHorizon,1);
    regWContFix = zeros(previewHorizon,1);
    
    Q = zeros(n,n,T);
    R = zeros(m,m,T);
    w = 1*randn(n,T);
    for t = 1:T
        Q(:,:,t) = (3+rand(1))*eye(n);
        R(:,:,t) = (5+rand(1))*eye(m);
    end

    %generate random system
    A = zeros(n,n);
    B = zeros(n,m);
    count = 1;
    while(rank(ctrb(A,B)) < n || count ~= 0)
        try
            count = 0;
            A = 10*rand(n,n);
            B = 10*rand(n,m);
            K0 = place(A,B,[1,6,4,3]*10^(-3));
        catch
            count = 1;
        end
    end
    x0 = 10*randn(n,1);
    %inverted pendulum system
%     M = 0.5;
%     g = 9.8;
%     l = 0.3;
%     mass = 0.2;
%     I = 0.006;
%     b = 0.1;
%     den = (I*(M+mass)+M*mass*l^2);
%     a22 = -(I+mass*l^2)*b/den;
%     a23 = mass^2*g*l^2/den;
%     a42 = -mass*l*n/den;
%     a43 = mass*g*l*(M+mass)/den;
% 
%     A = [0,1,0,0;0,a22,a23,0;0,0,0,1;0,a42,a43,0];
%     B = [0;(I+mass*l^2)/den;0;mass*l/den];
%     Pmax = findPmax((1.2*10^4+2*10^4)*eye(n),(4.8*10^4+5*10^4)*eye(m),A,B);
%     x0 = randn(n,1);
%     K0 = place(A,B,[1,6]*10^(-3));
    Pmax = findPmax(4*eye(n),6*eye(m),A,B);

    strInd = 0;
    for t = previewHorizon:T
        [xOptimal,uOptimal] = optimalCont(Q,R,A,B,w,t,x0,n,m);
        for W = strInd:previewHorizon-1
            [x,u] = expOptimalLina(A,B,Q,R,t,x0,n,m,w,W,Pmax);
            regWLi(W+1,t) = regT(x,u,xOptimal,uOptimal,Q,R,t);
            [x1,u1,x2,u2] = expOptimal(A,B,Q,R,t,x0,n,m,w,W,-K0,Pmax);
            regWMeFix(W+1,t) = regT(x1,u1,xOptimal,uOptimal,Q,R,t);
        end
    end
    regAvgMeFix = regAvgMeFix + regWMeFix;
    regAvgLi = regAvgLi + regWLi;
end

figure
surf(previewHorizon:T,strInd:previewHorizon-1,log10(1+abs(regAvgMeFix(:,previewHorizon:T))),'FaceColor','b')
hold on
surf(previewHorizon:T,strInd:previewHorizon-1,log10(1+abs(regAvgLi(:,previewHorizon:T))),'FaceColor','g')

figure
imagesc(log(1+abs(regAvgMeFix(:,previewHorizon:T)))-log(1+abs(regAvgLi(:,previewHorizon:T))))


compMatrix = regAvgMeFix(:,previewHorizon:T)-regAvgLi(:,previewHorizon:T);
(sum(compMatrix<0,'all'))/(length(previewHorizon:T)*previewHorizon)

save("RandomSys1NoiseMe200.mat",'regAvgMeFix');
save("RandomSys1NoiseLi200.mat",'regAvgLi');

%%
clear all
T = 500;
n = 4;
m = 1;

noiseHorizon = 100;
previewHorizon = 20;

regWMeFix = zeros(previewHorizon,T);
regWLi = zeros(previewHorizon,T);
regAvgMeFix = zeros(previewHorizon,T);
regAvgLi = zeros(previewHorizon,T);
regContFix = zeros(previewHorizon,T);

%%
numMonte = 200;

epsilon = 100;
for numExp = 1:numMonte
    regWMeFix = zeros(previewHorizon,1);
    regWLi = zeros(previewHorizon,1);
    regWContFix = zeros(previewHorizon,1);
    
    Q = zeros(n,n,T);
    R = zeros(m,m,T);
    w = 0*randn(n,T);
    for t = 1:T
        Q(:,:,t) = (3+rand(1))*eye(n);
        R(:,:,t) = (5+rand(1))*eye(m);
    end

    %generate random system
%     A = zeros(n,n);
%     B = zeros(n,m);
    count = 1;
%     while(rank(ctrb(A,B)) < n || count ~= 0)
%         try
%             count = 0;
%             A = 10*rand(n,n);
%             B = 10*rand(n,m);
%             K0 = place(A,B,[1,6,4,3]*10^(-3));
%         catch
%             count = 1;
%         end
%     end
    x0 = 10*randn(n,1);
    %inverted pendulum system
    M = 0.5;
    g = 9.8;
    l = 0.3;
    mass = 0.2;
    I = 0.006;
    b = 0.1;
    den = (I*(M+mass)+M*mass*l^2);
    a22 = -(I+mass*l^2)*b/den;
    a23 = mass^2*g*l^2/den;
    a42 = -mass*l*n/den;
    a43 = mass*g*l*(M+mass)/den;

    A = [0,1,0,0;0,a22,a23,0;0,0,0,1;0,a42,a43,0];
    B = [0;(I+mass*l^2)/den;0;mass*l/den];
    K0 = place(A,B,[1,6,4,3]*10^(-3));
%     Pmax = findPmax((1.2*10^4+2*10^4)*eye(n),(4.8*10^4+5*10^4)*eye(m),A,B);
%     x0 = randn(n,1);
%     K0 = place(A,B,[1,6]*10^(-3));
    Pmax = findPmax(4*eye(n),6*eye(m),A,B);
    strInd = 0;
    for t = previewHorizon:T
        [xOptimal,uOptimal] = optimalCont(Q,R,A,B,w,t,x0,n,m);
        for W = strInd:previewHorizon-1
            [x,u] = expOptimalLina(A,B,Q,R,t,x0,n,m,w,W,Pmax);
            regWLi(W+1,t) = regT(x,u,xOptimal,uOptimal,Q,R,t);
            [x1,u1,x2,u2] = expOptimal(A,B,Q,R,t,x0,n,m,w,W,-K0,Pmax);
            regWMeFix(W+1,t) = regT(x1,u1,xOptimal,uOptimal,Q,R,t);
        end
    end
    regAvgMeFix = regAvgMeFix + regWMeFix;
    regAvgLi = regAvgLi + regWLi;
end


figure
surf(previewHorizon:T,strInd:previewHorizon-1,(regAvgMeFix(:,previewHorizon:T)),'EdgeColor','b')
hold on
surf(previewHorizon:T,strInd:previewHorizon-1,(regAvgLi(:,previewHorizon:T)),'EdgeColor','g')

figure
surf(previewHorizon:T,strInd:previewHorizon-1,log10(1+abs(regAvgMeFix(:,previewHorizon:T))),'EdgeColor','interp')
hold on
surf(previewHorizon:T,strInd:previewHorizon-1,log10(1+abs(regAvgLi(:,previewHorizon:T))),'EdgeColor','interp')

compMatrix = regAvgMeFix(:,previewHorizon:T)-regAvgLi(:,previewHorizon:T);
(sum(compMatrix<0,'all'))/(length(previewHorizon:T)*previewHorizon)

save('PenSysNoNoiseMe200',"regAvgMeFix");
save('PenSysNoNoiseLi200',"regAvgLi");




