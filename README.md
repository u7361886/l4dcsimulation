# Regret Analysis of Online LQR Control via Trajectory Prediction and Tracking
The paper can be found at 
- [ ] [Proceedings of Machine Learning Research (PMLR) / Learning for Dynamics & Control (L4DC) 2023]()
- [ ] [arXiv(extension)](https://arxiv.org/abs/2302.10411)


## Dependencies

- Matlab

## Installation and usage

Simply download this repository and run file simulationOptimalFinal.m

## Results

Below are simulation results from the paper. Details can be found from Section 4 Numerical Results https://arxiv.org/abs/2302.10411

Disturbanceless Inverted Pendulum Systems
<center>
    <img src="Results/PhysSys.png" width="450"/>
    <!---<figcaption>PhysSystem</figcaption>--->
</center>

Disturbanceless Random Linear Systems

[<img src="Results/RandomSys.png" width="450"/>](image.png)

Inverted Pendulum Systems with Disturbance

[<img src="Results/PhysSysMonteNoise.png" width="450"/>](image.png)

Random Systems with Disturbance

[<img src="Results/RandSysMonteNoise.png" width="450"/>](image.png)

## Authors
- Yitian Chen Australian National University
- Timothy Molloy [Australian National University](https://eng.anu.edu.au/people/timothy-molloy/)
- Tyler Summers [University of Texas at Dallas
](https://personal.utdallas.edu/~tyler.summers/)
- Iman Shames [Australian National University](https://researchers.anu.edu.au/researchers/shames-i)

