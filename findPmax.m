function P = findPmax(Q,R,A,B)

T = 10^3;
P = eye(size(Q));
for t = 1:T
    P = Q + A'*P*A-A'*P*B*inv(R+B'*P*B)*B'*P*A;
end

