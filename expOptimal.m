function [x1,u1,x2,u2] = expOptimal(A,B,Q,R,T,x0,n,m,w,W,K0,Pmax)
    x1 = zeros(n,T);
    u1 = zeros(m,T);
    x1(:,1) = x0;

    x2 = zeros(n,T);
    u2 = zeros(m,T);
    x2(:,1) = x0;
    
    P = zeros(n,n,T);
    K = zeros(m,n,T-1);
    
    for t = 1:T-1
        ind = (t+W)*(t+W <= T) + T*(t+W>T);
        if(ind == T)
            P(:,:,T) = Q(:,:,T);
        else
%             P(:,:,ind+1) = Pmax;
            P(:,:,T) = Q(:,:,ind);
        end
    
        for i = T-1:-1:1
            if(i > ind)
                tempR = R(:,:,ind);   %R in index i with given information at t
                tempQ = Q(:,:,ind);
            end
            if(i <= ind)
                tempR = R(:,:,i);
                tempQ = Q(:,:,i);
            end
              K(:,:,i) = -inv(tempR+(B')*P(:,:,i+1)*B)*(B')*P(:,:,i+1)*A;
              P(:,:,i) = (A')*P(:,:,i+1)*A+tempQ+(A')*P(:,:,i+1)*B*K(:,:,i);
        end
        
        x_tcond = x0;
        for j = 1:t-1
            x_tcond = (A+B*K(:,:,j))*x_tcond + w(:,j);
        end
%         size(x_tcond)
%         size(K(:,:,1))
%         tempX = eye(n);
%         for j = t-1:-1:1
% %               tempX = tempX*(A+B*K(:,:,j));
%         end
%         tempXt = tempX*x0;
        u1(:,t) = K0*(x1(:,t)-x_tcond) + K(:,:,t)*x_tcond;
        x1(:,t+1) = A*x1(:,t)+B*u1(:,t)+w(:,t);

        u2(:,t) = K(:,:,t)*x2(:,t);
        x2(:,t+1) = A*x2(:,t)+B*u2(:,t)+w(:,t);
    end
end



% function [x,u] = expOptimal(A,B,Q,R,T,x0,n,m,w,W,K0,Pmax)
% 
% x = zeros(n,T);
% u = zeros(m,T);
% x(:,1) = x0;
% % xOptimal = zeros(n,T);
% % uOptimal = zeros(m,T);
% % 
% % POptimal = zeros(n,n,T);
% % POptimal(:,:,T) = Q(:,:,T);
% % KOptimal = zeros(m,n,T-1);
% % 
% % for t = T-1:-1:1
% %     KOptimal(:,:,t) = -inv(R(:,:,t)+(B')*POptimal(:,:,t+1)*B)*(B')*POptimal(:,:,t+1)*A;
% %     POptimal(:,:,t) = (A')*POptimal(:,:,t+1)*A+Q(:,:,t)+(A')*POptimal(:,:,t+1)*B*KOptimal(:,:,t);
% % end
% % for t = 1:T-1
% %    uOptimal(:,t) = KOptimal(:,:,t)*xOptimal(:,t) - inv(R(:,:,t)+(B')*POptimal(:,:,t+1)*B)*(B')*POptimal(:,:,t+1)*w(:,t);
% %    xOptimal(:,t+1) = A*xOptimal(:,t)+B*uOptimal(:,t)+w(:,t);
% % end
% 
% P = zeros(n,n,T);
% K = zeros(m,n,T-1);
% 
% for t = 1:T-1
% %     P(:,:,T) = Q(:,:,t);
%     ind = (t+W)*(t+W <= T) + T*(t+W>T);
% %     ind = T;
% 
% %     P(:,:,T) = Q(:,:,ind);
%     if(ind == T)
%         P(:,:,ind) = Q(:,:,T);
%     else
% %         P(:,:,T) = Q(:,:,ind);
%         P(:,:,ind) = Pmax;
%     end
% %     
%     for i = ind-1:-1:1
%         if(i > ind)
%             tempR = R(:,:,ind);   %R in index i with given information at t
%             tempQ = Q(:,:,ind);
%         end
%         if(i <= ind)
%             tempR = R(:,:,i);
%             tempQ = Q(:,:,i);
%         end
%           K(:,:,i) = -inv(tempR+(B')*P(:,:,i+1)*B)*(B')*P(:,:,i+1)*A;
%           P(:,:,i) = (A')*P(:,:,i+1)*A+tempQ+(A')*P(:,:,i+1)*B*K(:,:,i);
%     end
%     tempX = eye(n);
%     for j = t-1:-1:1
%           tempX = tempX*(A+B*K(:,:,j));
%     end
%     tempXt = tempX*x0;
%     u(:,t) = K0*(x(:,t)-tempXt) + K(:,:,t)*tempXt;
% %     if(W > 0)
% %         Kc = K(:,:,t);
% %     else
% %         Kc = K0;
% %     end
% %     u(:,t) = Kc*x(:,t);
%     x(:,t+1) = A*x(:,t)+B*u(:,t)+w(:,t);
% end
% 
% % reg = zeros(T,1);
% % 
% % 
% % for t = 1:T-1
% %    if(t == 1)
% %         reg(t) = (x(:,t)')*Q(:,:,t)*x(:,t)-((xOptimal(:,t)')*Q(:,:,t)*xOptimal(:,t));
% %    else
% %         reg(t+1) = reg(t) + (x(:,t)')*Q(:,:,t)*x(:,t)+ (u(:,t-1)')*R(:,:,t-1)*u(:,t-1)-((xOptimal(:,t)')*Q(:,:,t)*xOptimal(:,t)+ (uOptimal(:,t-1)')*R(:,:,t-1)*uOptimal(:,t-1));
% %    end
% % end
% % 
% % out = reg;
% % figure
% % plot(20*log10(reg+1))
% 
% end
