function [x,u] = expOptimalLina(A,B,Q,R,T,x0,n,m,w,W,Pmax)

%     xOptimal = zeros(n,T);
%     uOptimal = zeros(m,T);
%     
%     POptimal = zeros(n,n,T);
%     POptimal(:,:,T) = Q(:,:,T);
%     KOptimal = zeros(m,n,T-1);
%     
%     for t = T-1:-1:1
%         KOptimal(:,:,t) = -inv(R(:,:,t)+(B')*POptimal(:,:,t+1)*B)*(B')*POptimal(:,:,t+1)*A;
%         POptimal(:,:,t) = (A')*POptimal(:,:,t+1)*A+Q(:,:,t)+(A')*POptimal(:,:,t+1)*B*KOptimal(:,:,t);
%     end
%     for t = 1:T-1
%        uOptimal(:,t) = KOptimal(:,:,t)*xOptimal(:,t) - inv(R(:,:,t)+(B')*POptimal(:,:,t+1)*B)*(B')*POptimal(:,:,t+1)*w(:,t);
%        xOptimal(:,t+1) = A*xOptimal(:,t)+B*uOptimal(:,t)+w(:,t);
%     end

    x = zeros(n,T);
    u = zeros(m,T);
    x(:,1) = x0;
    
    
    if(W == 0)
        K0 = -inv(B'*Pmax*B)*B'*Pmax*A;
        for t = 1:T-1
            u(:,t) = K0*x(:,t);
            x(:,t+1) = A*x(:,t) + B*u(:,t) + w(:,t);
        end
    else
        P = zeros(n,n,W+1);
        K = zeros(m,n,W);
        for t = 1:T-W
            if(t+W >= T)
                P(:,:,W+1) = Q(:,:,T);
            else
                P(:,:,W+1) = Pmax;
            end
            for i = W:-1:1
                  K(:,:,i) = -inv(R(:,:,i+t-1)+(B')*P(:,:,i+1)*B)*(B')*P(:,:,i+1)*A;
                  P(:,:,i) = (A')*P(:,:,i+1)*A+Q(:,:,i+t-1)+(A')*P(:,:,i+1)*B*K(:,:,i);
            end
            u(:,t) = K(:,:,1)*x(:,t);
            x(:,t+1) = A*x(:,t) + B*u(:,t) + w(:,t);
        end
        for t = T-W+1:T-1
            u(:,t) = K(:,:,t-(T-W-1))*x(:,t);
    %         x(:,t+1) = (A+B*K(:,:,t-(T-W-1)))*x(:,t) + w(:,t);
            x(:,t+1) = A*x(:,t) + B*u(:,t) + w(:,t);
        end
    end
    
%     reg = zeros(T,1);
%     
%     for t = 1:T-1
%        if(t == 1)
%             reg(t) = (x(:,t)')*Q(:,:,t)*x(:,t)-((xOptimal(:,t)')*Q(:,:,t)*xOptimal(:,t));
%        else
%             reg(t+1) = reg(t) + (x(:,t)')*Q(:,:,t)*x(:,t)+ (u(:,t-1)')*R(:,:,t-1)*u(:,t-1)-((xOptimal(:,t)')*Q(:,:,t)*xOptimal(:,t)+ (uOptimal(:,t-1)')*R(:,:,t-1)*uOptimal(:,t-1));
%        end
%     end
%     
%     out = reg;
end
