function [xOptimal,uOptimal] = optimalCont(Q,R,A,B,w,t,x0,n,m)
    xOptimal = zeros(n,t);
    xOptimal(:,1) = x0;
    uOptimal = zeros(m,t-1);
    K = zeros(m,n,t);
    P = Q(:,:,t);
    for p = t-1:-1:1
        K(:,:,p) = -inv(R(:,:,p)+(B')*P*B)*(B')*P*A;
        P = (A')*P*A+Q(:,:,p)+A'*P*B*K(:,:,p);
    end

    for p = 1:t-1
        uOptimal(:,p) = K(:,:,p)*xOptimal(:,p);
        xOptimal(:,p+1) = A*xOptimal(:,p)+B*uOptimal(:,p)+w(:,p);
    end

end